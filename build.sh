#!/usr/bin/env bash
set -e # exit when any command fails

REF=${CONTAINER_IMAGE}:latest

if [[ ${CI_BUILD_REF_NAME} == 'master' ]]; then
    REF=${CONTAINER_IMAGE_MASTER}
fi

# Build
docker login -u gitlab-ci-token -p ${CI_BUILD_TOKEN} registry.gitlab.com
docker build -t ${REF} .

# Test
docker run --rm ${REF} ansible-lint --version

# Push
if [[ $? == 0 ]]; then
    docker push ${REF}
fi
