FROM alpine:latest

RUN apk add --no-cache bash python python2-dev py-pip gcc musl-dev libffi-dev openssl-dev && \
    pip install ansible-lint
